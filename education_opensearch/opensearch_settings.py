import os

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 19200)
ES_USER = os.getenv("ES_USER", "admin")
ES_PWD = os.getenv("ES_PWD", "admin")
ES_USE_SSL = os.getenv('ES_USE_SSL', 'false').lower() == 'true'
ES_VERIFY_CERTS = os.getenv('ES_VERIFY_CERTS', 'false').lower() == 'true'

# The index (or alias to indicies) to query against:
ES_EDUCATIONS_ALIAS = os.getenv('ES_EDUCATIONS_ALIAS', 'educations-merged-enriched')

VERSION = '0.0.1'

education_mappings = {
    "mappings": {
        "properties": {
            "id": {
                "type": "keyword",
            }
        }
    }
}