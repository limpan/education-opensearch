import pytest

from education_opensearch.opensearch_store import OpensearchStore
from datetime import datetime

@pytest.mark.unit
def test_store_doc():
    opensearch_store = OpensearchStore()
    es_index = opensearch_store.start_new_save('unit-test')
    opensearch_store.save_education_to_repository({'id': datetime.now().strftime('%Y%m%d-%H.%M.%S'), 'test': 'test'})
    assert es_index is not None
