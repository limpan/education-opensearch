# Education opensearch

Project for using Open search db as storage

## prerequisite
- Make sure to be logged on to jobtech nexus:
```
docker login docker-images.jobtechdev.se
```

## Install Opensearch localhost on podman or docker desktop:
This installs latest opensearch image with no authentication, e.g. username/password dont need to be provided when querying.
Prerequisite: Install podman + podman-compose or docker desktop.

### 1. Build a new dashboard
This disables the security plugin and builds a new image
tagged opensearch-dashboards-no-security.
#### If Podman:
- In a shell move to [this repo]/docker/
- Run `podman build --tag=opensearch-dashboards-no-security .`
###
- In a shell move to [this repo]/docker/
- Run `docker build --tag=opensearch-dashboards-no-security .`

### 2. Run opensearch:
This uses the new dashboard image previoudly built.
Starts the containers in detached mode.
#### If Podman:
- Run: `podman-compose up -d`
#### If Docker:
- Run: `docker-compose up -d`

Verify start up by surfing into Opensearch Dashboards in a browser: localhost:5601

### Stopping
To stop and remove containers
#### If Podman:
- Run: `podman-compose down`

#### If Docker:
- Run: `docker-compose down`

### Troubleshoot docker for desktop on Windows:
If open search fails to start in docker for desktop on Windows due to 'vm.max_map_count [65530] is too low':

- In Powershell as admin:
```
docker-machine ssh
sudo sysctl -w vm.max_map_count=262144
```

## Run tests against localhost
- Make sure Python 3.8 or higher is installed
- Activate a virtual env (e.g. conda or virtualenv)
- Default settings in settings.py should work for local open search, otherwise set using env.variables.
- If using Pycharm/IntelliJ make sure pytest is set as default test runner:
File -> Settings -> Python integrated tools -> testing -> default test runner -> pytest
- 
- Run tests in tests dir

## Run tests against remote AWS test dev
- Make sure Python 3.8 or higher is installed
- Activate a virtual env (e.g. conda or virtualenv)
-  Set the following env.variables that are specified in settings.py:
```
ES_HOST=search-jobtech-opensearch-7zrtdhvq4feysy3peubhzsweai.eu-central-1.es.amazonaws.com;ES_USER=[ask for];ES_PORT=443;ES_PWD=[ask for]
```
- If using Pycharm/IntelliJ make sure pytest is set as default test runner:
  File -> Settings -> Python integrated tools -> testing -> default test runner -> pytest
- 
- Run tests in tests dir
